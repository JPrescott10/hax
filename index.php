<?php

?>

<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        height: 400px;
        width: 100%;
       }
    </style>
  </head>
  <body>
    <h3>Pub Crawl!</h3>
    <div id="map"></div>
	
	This is our pub crawl generator... <br><br>

	Input Start Location: <input id="start" type="text" required/><br>

	Input Number of Stops: <input id="numStops" type="int" required/><br>

	<a href="script/python.php"> Run Calculations </a>

    <script>
      function initMap() {
        var melb = {lat: -37.815018, lng: 144.946014};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: melb
        });
  //      var marker = new google.maps.Marker({
   //       position: melb,
   //       map: map
   //     });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUXuFQ7wCrLSmzzQW0C7bqoO1WO3qnGb0&callback=initMap">
    </script>
  </body>
</html>